# python3.6 + pip3.6

sudo add-apt-repository ppa:jonathonf/python-3.6
sudo apt update
sudo apt install python3.6
sudo apt install python3.6-dev
sudo apt install python3.6-venv
sudo ln -s /usr/bin/python3.6 /usr/local/bin/python3
sudo wget https://bootstrap.pypa.io/ez_setup.py -O - | sudo python3.6
sudo curl https://bootstrap.pypa.io/get-pip.py | sudo python3.6
sudo (pip -V && pip3 -V && pip3.6 -V) | uniq
